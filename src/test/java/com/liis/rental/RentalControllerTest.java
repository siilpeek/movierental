package com.liis.rental;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.liis.rental.entity.Movie;
import com.liis.rental.entity.RentEvent;
import com.liis.rental.entity.User;
import com.liis.rental.service.RentalService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RentalControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@MockBean
	private  RentalService rentalService;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.build();
	}

	@Test
	public void givenMovieIsRented_whenGetRentHistoryIsRequested_thenAllRentHistoryIsReturned() throws Exception {
		List<RentEvent> allRentEvents = returnListOfTestRentals();
		given(rentalService.getAllRentEvents()).willReturn(allRentEvents);

		mockMvc.perform(get("/renthistory")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].movie.title").value(allRentEvents.get(0).getMovie().getTitle()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].user.id").value(allRentEvents.get(0).getUser().getId()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].returnDate").value(allRentEvents.get(0).getReturnDate().toString()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].borrowedDate").value(allRentEvents.get(0).getBorrowedDate().toString()));

	}

	@Test
	public void givenMovieIsCreated_whenRentMovieIsRequested_thenAllRentEventIsReturned() throws Exception {
		Movie testRomanticMovie = new Movie(1, "Forrest Gump", "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.", List.of("Tom Hanks", "Robin Wright"),LocalDate.of( 1994 , 2 , 11 ));
		User testUser = new User(1, "testFirstName", "testLastName", "test@test.ee", "testAddress", "password", "username");
		RentEvent rentEventForUserMovie1 = new RentEvent(1, testUser, LocalDate.of( 2023 , 2 , 11 ), LocalDate.of( 2023 , 2 , 15 ), true, testRomanticMovie, 1);
		given(rentalService.rentMovie(testUser.getId(),testRomanticMovie.getId(),3)).willReturn(rentEventForUserMovie1);

		MvcResult mvcResult = mockMvc.perform(put("/rent/movie/" +testRomanticMovie.getId() + "/user=" + testUser.getId() + "&weeks=" + 3)
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(rentEventForUserMovie1)))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void givenUserWantsToReturnMovie_whenReturnMovieIsRequested_thenAllRentEventIsReturned() throws Exception {
		Movie testRomanticMovie = new Movie(1, "Forrest Gump", "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.", List.of("Tom Hanks", "Robin Wright"),LocalDate.of( 1994 , 2 , 11 ));
		User testUser = new User(1, "testFirstName", "testLastName", "test@test.ee", "testAddress", "password", "username");
		RentEvent rentEventForUserMovie1 = new RentEvent(1, testUser, LocalDate.of( 2023 , 2 , 11 ), LocalDate.of( 2023 , 2 , 15 ), true, testRomanticMovie, 1);
		given(rentalService.returnMovie(testUser.getId(),testRomanticMovie.getId())).willReturn(rentEventForUserMovie1);

		MvcResult mvcResult = mockMvc.perform(put("/rent/movie/return/user=" +testUser.getId() + "&movie=" + testRomanticMovie.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(rentEventForUserMovie1)))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void givenUserHasActiveRents_whenInvoiceIsRequested_thenTotalAmountForAllRentsIsCalculated() throws Exception {
		User testUser = new User(1, "testFirstName", "testLastName", "test@test.ee", "testAddress", "password", "username");
		given(rentalService.getInvoice(testUser.getId())).willReturn(5);

		mockMvc.perform(get("/invoice/user=" + testUser.getId())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$").value(5));
	}

	public static String asJsonString(final Object obj) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private List<RentEvent> returnListOfTestRentals(){
		Movie testRomanticMovie = new Movie(1, "Forrest Gump", "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.", List.of("Tom Hanks", "Robin Wright"),LocalDate.of( 1994 , 2 , 11 ));
		Movie testScaryMovie = new Movie(2, "The Shining", "A family heads to an isolated hotel for the winter where a sinister presence influences the father into violence, while his psychic son sees horrific forebodings from both past and future.", List.of("Jack Nicholson", "Shelley Duvall"),LocalDate.of( 1980 , 2 , 11 ));
		User testUser = new User(1, "testFirstName", "testLastName", "test@test.ee", "testAddress", "password", "username");
		RentEvent rentEventForUserMovie1 = new RentEvent(1, testUser, LocalDate.of( 2023 , 2 , 11 ), LocalDate.of( 2023 , 2 , 15 ), true, testRomanticMovie, 1);
		RentEvent rentEventForUserMovie2 = new RentEvent(1, testUser, LocalDate.of( 2023 , 2 , 11 ), LocalDate.of( 2023 , 2 , 15 ), true, testScaryMovie, 1);
		List<RentEvent> allRentEvents = new ArrayList<>();
		allRentEvents.add(rentEventForUserMovie1);
		allRentEvents.add(rentEventForUserMovie2);
		return allRentEvents;
	}

}
