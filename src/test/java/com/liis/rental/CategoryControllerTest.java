package com.liis.rental;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.liis.rental.entity.Category;
import com.liis.rental.service.CategoryService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CategoryControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private  CategoryService categoryService;

	@Test
	public void whenCategoriesAreRequested_thenAllCategoriesAreReturned() throws Exception 
	{
		List<Category> allCategories = returnCollectionOfTestCategories();
		given(categoryService.getAllCategories()).willReturn(allCategories);
		
		mvc.perform(MockMvcRequestBuilders
				.get("/categories")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(allCategories.get(0).getName()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].categoryId").value(allCategories.get(0).getCategoryId()))
		.andDo(MockMvcResultHandlers.print());
	}
	
	private List<Category> returnCollectionOfTestCategories() {
		Category testRomanticCategory = new Category(2, "Scary movies");
		List<Category> allCategories = new ArrayList<>();
		allCategories.add(testRomanticCategory);
		return allCategories;
	}
}
