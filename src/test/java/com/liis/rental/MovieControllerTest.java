package com.liis.rental;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.liis.rental.entity.Movie;
import com.liis.rental.service.MovieService;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@MockBean
	private  MovieService movieService;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.build();
	}

	@Test
	public void givenMovieIsAdded_whenGetMoviesIsRequested_thenJsonArrayIsReturned() throws Exception {
		List<Movie> allMovies = returnListOfTestMovies();
		given(movieService.getAllMovies()).willReturn(allMovies);

		mockMvc.perform(get("/movies")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(allMovies.get(0).getTitle()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].actors").isArray())
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(allMovies.get(0).getDescription()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].releaseDate").value(allMovies.get(0).getReleaseDate().toString()));
	
	}

	private List<Movie> returnListOfTestMovies(){
		Movie testRomanticMovie = new Movie(1, "Forrest Gump", "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.", List.of("Tom Hanks", "Robin Wright"),LocalDate.of( 1994 , 2 , 11 ));
		Movie testScaryMovie = new Movie(2, "The Shining", "A family heads to an isolated hotel for the winter where a sinister presence influences the father into violence, while his psychic son sees horrific forebodings from both past and future.", List.of("Jack Nicholson", "Shelley Duvall"),LocalDate.of( 1980 , 2 , 11 ));
		List<Movie> allMovies = new ArrayList<>();
		allMovies.add(testRomanticMovie);
		allMovies.add(testScaryMovie);
		return allMovies;
	}
}
