package com.liis.rental;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liis.rental.entity.User;
import com.liis.rental.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@MockBean
	private  UserService userService;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.build();
	}
	
    @Test
    public void givenUserExists_whenEmpRequestsToReciveUsers_thenUserIsReceived() throws Exception {
        User testUser = new User(1, "Triin", "Filmisõber", "triin@email.ee", "Paberi 4-5", "testusername", "testpassword");

        given(userService.searchUserById(testUser.getId())).willReturn(testUser);

        mockMvc.perform(get("/user/" + testUser.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(testUser.getId()));
    }

    @Test
    public void whenEmpRequestsToGetUsers_thenAllUsersAreReceived() throws Exception {
    	List<User> allUsers = returnListOfTestUsers();
		given(userService.getAllUsers()).willReturn(allUsers);

		mockMvc.perform(get("/users")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value(allUsers.get(0).getFirstName()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value(allUsers.get(0).getLastName()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].email").value(allUsers.get(0).getEmail()))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].address").value(allUsers.get(0).getAddress()));
    }

    private List<User> returnListOfTestUsers() {
		User testUser = new User(1, "testFirstName", "testLastName", "test@test.ee", "testAddress", "password", "username");
		User testUser2 = new User(2, "Triin", "Filmisõber", "triin@email.ee", "Paberi 4-5", "testusername", "testpassword");
		List<User> allTestUsers = new ArrayList<>();
		allTestUsers.add(testUser);
		allTestUsers.add(testUser2);
		return allTestUsers;
	}

	public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
