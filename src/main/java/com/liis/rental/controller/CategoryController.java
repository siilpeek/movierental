package com.liis.rental.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.liis.rental.entity.Category;
import com.liis.rental.service.CategoryService;

@RestController
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/categories")
	public Collection<Category> findAllCategorues(){
		return categoryService.getAllCategories();
	}

	@PostMapping("/category/create")
	public Category createCategory(@RequestBody Category category) throws FileNotFoundException, IOException {
		return categoryService.addNewCategory(category);
	}

	@PutMapping("/category/{categoryId}/addmovie/{movieId}")
	public Category addMovieToCategory(@PathVariable (value = "categoryId") Integer categoryId,
			@PathVariable (value = "movieId") Integer movieId) throws FileNotFoundException, IOException {
		return categoryService.addMovieToCategory(categoryId, movieId);
	}
}
