package com.liis.rental.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.liis.rental.entity.Movie;
import com.liis.rental.entity.RentEvent;
import com.liis.rental.service.RentalService;

@RestController
public class RentalController {

	@Autowired
	private RentalService rentalService;

	@PutMapping("/rent/movie/{movieId}/user={userId}&weeks={rentalWeeks}")
	public RentEvent rentMovie(@PathVariable (value = "movieId") Integer movieId,
			@PathVariable (value = "userId") Integer userId,
			@PathVariable (value = "rentalWeeks") Integer rentalWeeks) throws Exception {
		return rentalService.rentMovie(userId, movieId, rentalWeeks);
	}
	
	@PutMapping("/rent/movie/return/user={userId}&movie={movieId}")
	public RentEvent returnMovie(@PathVariable (value = "userId") Integer userId,
			@PathVariable (value = "movieId") Integer movieId) throws Exception {
		return rentalService.returnMovie(userId, movieId);
	}

	@GetMapping("/invoice/user={userId}")
	public double calculateInvoice(
			@PathVariable (value = "userId") Integer userId) throws Exception {
		return rentalService.getInvoice(userId);
	}

	@GetMapping("/renthistory")
	public Collection<RentEvent> findAlRentalEvents(){
		return rentalService.getAllRentEvents();
	}
	
	@GetMapping("/rent/statistics/{searchCriteria}")
	public List<Movie> getStatisticsForPopularAndNotPopularMovies(
			@PathVariable (value = "searchCriteria") String searchCriteria){
		return rentalService.getStatisticsForPopularAndNotPopularMovies(searchCriteria);
	}
	
	@GetMapping("/rent/statistics/averages/{searchCriteria}")
	public double getStatisticsForAverageRentalTimesAndPrices(
			@PathVariable (value = "searchCriteria") String searchCriteria){
		return rentalService.getAveragesForRentalPriceAndRentalTimes(searchCriteria);
	}
}
