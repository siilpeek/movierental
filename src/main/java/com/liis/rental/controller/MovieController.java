package com.liis.rental.controller;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.liis.rental.entity.Movie;
import com.liis.rental.service.MovieService;

@RestController
public class MovieController {

	@Autowired
	private MovieService movieService;

	@GetMapping("/movies")
	public Collection<Movie> findAllMovies(){
		return movieService.getAllMovies();
	}

	@GetMapping("/movies/{movieId}")
	public Movie getMovieById(@PathVariable (value = "movieId") Integer movieId){
		return movieService.findMovieById(movieId);
	}

	@PostMapping("/movie/add")
	public Movie addMovie(@RequestBody Movie movie) throws StreamWriteException, IOException {
		return movieService.createMovie(movie);
	}

	@PutMapping("/movie/update/{moveId}")
	public Movie updateMovie(@PathVariable (value = "movieId") Integer movieId,
			@RequestBody Movie movieRequest) {
		return movieService.updateMovie(movieId, movieRequest);
	}
	
	@DeleteMapping("/movie/delete/{moveId}")
	public ResponseEntity<?> deleteMovie(@PathVariable (value = "movieId") Integer movieId) {
		return movieService.deleteMovie(movieId);
	}
}