package com.liis.rental.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.liis.rental.entity.User;
import com.liis.rental.service.UserService;

@RestController
public class UserController {
	
	static Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/user/add")
    public User addUser(@RequestBody User user) throws StreamWriteException, IOException{
        User addedUser = userService.addUser(user.getId(), user);
        logger.log(Level.INFO, "Request to add new user was received. Added user is " + addedUser.toString());
        return addedUser;
    }
	
    @GetMapping("/user/{userId}")
    public User getUserById(@PathVariable Integer userId) throws Exception{
        User receivedUser = userService.searchUserById(userId);
        logger.log(Level.INFO, "Request to show a singe user was received. User shown is " + receivedUser.toString());
        return receivedUser;
    }
    
	@GetMapping("/users")
	public Collection<User> findAllUsers(){
		return userService.getAllUsers();
	}
	
		@GetMapping("/welcome")
		public String hello(){
			return "Hello";
		}
}
