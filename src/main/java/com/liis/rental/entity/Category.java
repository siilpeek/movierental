package com.liis.rental.entity;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private int categoryId;
    private String name;
    private List<Movie> movies = new ArrayList<>();

	public Category(int categoryId, String name) {
		this.categoryId = categoryId;
		this.name = name;
	}

	public Category() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public int getCategoryId() {
		return categoryId;
	}

	@Override
	public String toString() {
		return "Category [category_id=" + categoryId + ", name=" + name + ", movies=" + movies + "]";
	}
}
