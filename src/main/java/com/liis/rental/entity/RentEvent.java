package com.liis.rental.entity;

import java.time.LocalDate;

public class RentEvent {
	
	private Integer id;
	private User user;
	private LocalDate borrowedDate;
	private LocalDate returnDate;
	private boolean active;
	private Movie movie;
	private double price;
	
	public RentEvent(Integer id, User user, LocalDate borrowedDate, LocalDate returnDate, boolean active, Movie movie, double price) {
		this.id = id;
		this.user = user;
		this.borrowedDate = borrowedDate;
		this.returnDate = returnDate;
		this.active = active;
		this.movie = movie;
		this.price = price;
	}

	public RentEvent() {
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDate getBorrowedDate() {
		return borrowedDate;
	}

	public void setBorrowedDate(LocalDate borrowedDate) {
		this.borrowedDate = borrowedDate;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "RentEvent [user=" + user + ", borrowedDate=" + borrowedDate + ", returnDate=" + returnDate + ", active="
				+ active + ", movie=" + movie + ", price=" + price + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
