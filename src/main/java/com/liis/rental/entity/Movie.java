package com.liis.rental.entity;
import java.time.LocalDate;
import java.util.List;

public class Movie {
	private int id;
	private String title;
	private String description;
	private List<String> actors;
	private LocalDate releaseDate;
	
	public Movie(int id, String title, String description, List<String> actors, LocalDate localDate) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.actors = actors;
		this.releaseDate = localDate;
	}

	public Movie() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	@Override
	public String toString() {
		return "Movie [id=" + id + ", title=" + title + ", description=" + description + ", actors=" + actors
				+ ", releaseDate=" + releaseDate;
	}
}
