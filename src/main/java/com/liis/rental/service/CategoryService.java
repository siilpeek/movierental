package com.liis.rental.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.liis.rental.entity.Category;
import com.liis.rental.entity.Movie;

@Service
public class CategoryService {

	@Autowired
	MovieService movieService;

	static Logger logger = Logger.getLogger(CategoryService.class.getName());

	Map<Integer, Category> categoryRepo = new HashMap<>();

	public Category addNewCategory(Category category) throws FileNotFoundException, IOException {
		categoryRepo.put(category.getCategoryId(), category);
		logger.log(Level.INFO, "Added one new category: " + category.toString());
		updateCategoryValuesForFile();
		return category;
	}

	public Category addMovieToCategory(Integer categoryId, Integer movieId) throws FileNotFoundException, IOException {
		if (!categoryRepo.containsKey(categoryId)) {
			throw new ResourceAccessException("Category with Id " + categoryId + " not found");
		}
		Category category = categoryRepo.get(categoryId);
		Movie movie = movieService.getMovieRepo().get(movieId);

		List<Movie> movies = category.getMovies();
		movies.add(movie);
		category.setMovies(movies);
		categoryRepo.replace(categoryId, category);
		updateCategoryValuesForFile();
		logger.log(Level.INFO, "Added one new movie (id: " + movieId  + ") to category: " + category.toString());
		return category;
	}

	public Collection<Category> getAllCategories() {
		return categoryRepo.values();
	}

	public Map<Integer, Category> getCategoryRepo() {
		return categoryRepo;
	}

	private void updateCategoryValuesForFile() throws FileNotFoundException, IOException {
		String dataResource = System.getProperty("datasource");
		if (dataResource == null) {
			return;
		}
		ObjectMapper mapper;
		File file;
		if (dataResource.equals("json")) {
			file = new File("src/main/resources/category.json");
			mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
			mapper.writeValue(g, categoryRepo);
			g.close();
		} else if (dataResource.equals("yaml")) {
			file = new File("src/main/resources/category.yaml");
			mapper = new ObjectMapper(new YAMLFactory());
			mapper.findAndRegisterModules();
			JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
			mapper.writeValue(g, categoryRepo);
			g.close();
		}
	}
}
