package com.liis.rental.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.liis.rental.entity.User;

@Service
public class UserService {

	Map<Integer, User> userRepo = new HashMap<>();

	static Logger logger = Logger.getLogger(UserService.class.getName());


	public Map<Integer, User> getUserRepo() {
		return userRepo;
	}

	public User addUser(Integer userId, User user) {
		userRepo.put(userId, user);
		logger.log(Level.INFO, "Added one new user: " + user.toString());
		updateUserValuesForFile();
		return user;
	}

	public User searchUserById(int userId) throws Exception{
		if (!userRepo.containsKey(userId)) {
			throw new Exception("User with Id " + userId + " not found");
		}
		return userRepo.get(userId);
	}

	public Collection<User> getAllUsers() {
		return userRepo.values();
	}

	public void setUserRepo(Map<Integer, User> userRepo) {
		this.userRepo = userRepo;
	}

	private void updateUserValuesForFile() {
		String dataResource = System.getProperty("datasource");
		if (dataResource == null) {
			return;
		}
		ObjectMapper mapper;
		File file;
		if (dataResource.equals("json")) {
			file = new File("src/main/resources/user.json");
			mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			try {
				JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
				mapper.writeValue(g, userRepo);
				g.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (dataResource.equals("yaml")) {
			file = new File("src/main/resources/user.yaml");
			mapper = new ObjectMapper(new YAMLFactory());
			mapper.findAndRegisterModules();
			try {
				JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
				mapper.writeValue(g, userRepo);
				g.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
