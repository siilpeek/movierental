package com.liis.rental.service;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.liis.rental.entity.Category;
import com.liis.rental.entity.Movie;
import com.liis.rental.entity.RentEvent;
import com.liis.rental.entity.User;

@Component
public class StartupApplicationListener implements 
ApplicationListener<ContextRefreshedEvent> {

	static Logger logger = Logger.getLogger(StartupApplicationListener.class.getName());

	@Autowired
	private UserService userService;

	@Autowired
	private MovieService movieService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private RentalService rentalService;

	public static int counter;

	@Override public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("Updating application with values from json files");

		String[] dataResources = {"user", "rent", "movie", "category"};
		
		String dataResource = System.getProperty("datasource");
		
		if (dataResource == null) {
			return;
		}
		
		switch (dataResource) {
		case "json" : 
			for(String resource : dataResources) {
				File filejson = new File("src/main/resources/" + resource + ".json");
				if (filejson.exists()) {
					readFromFile(resource, filejson, "json");
				}
			}
			break;
		case "yaml":
			for(String resource : dataResources) {
				File filejson = new File("src/main/resources/" + resource + ".yaml");
				if (filejson.exists()) {
					readFromFile(resource, filejson, "yaml");
				}
			}
			break;
			default:
				logger.info("Datasource not supported");
		}
	}

	private void readFromFile(String type, File file, String dataSource) {
		ObjectMapper mapper;
		if (dataSource == "json") {
			mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
		} else {
			mapper = new ObjectMapper(new YAMLFactory());
			mapper.findAndRegisterModules();
		}
		switch(type) {
		case "user":
			Map<Integer, User> alreadyExistingUsers = null; 
			try { 
				alreadyExistingUsers = mapper.readValue(file, new TypeReference<>(){}); 
			} catch (IOException e) { 
				e.printStackTrace(); 
			} 
			for (Entry<Integer,User> user : alreadyExistingUsers.entrySet()) {
				userService.userRepo.put(user.getKey(), user.getValue()); 
			} 
			break;
		case "movie":
			Map<Integer, Movie> alreadyExistingMovies = null; 
			try { 
				alreadyExistingMovies = mapper.readValue(file, new TypeReference<>(){}); 
			} catch (IOException e) { 
				e.printStackTrace(); 
			} 
			for (Entry<Integer,Movie> movie : alreadyExistingMovies.entrySet()) {
				movieService.movieRepo.put(movie.getKey(), movie.getValue()); 
			} 
			break;
		case "category":
			Map<Integer, Category> alreadyExistingCategories = null; 
			try { 
				alreadyExistingCategories = mapper.readValue(file, new TypeReference<>(){}); 
			} catch (IOException e) { 
				e.printStackTrace(); 
			} 
			for (Entry<Integer,Category> category : alreadyExistingCategories.entrySet()) {
				categoryService.categoryRepo.put(category.getKey(), category.getValue()); 
			} 
			break;
		case "rent":
			Map<String, RentEvent> alreadyExistingRentEvents = null; 
			try { 
				alreadyExistingRentEvents = mapper.readValue(file, new TypeReference<>(){}); 
			} catch (IOException e) { 
				e.printStackTrace(); 
			} 
			for (Entry<String, RentEvent> rentEvent : alreadyExistingRentEvents.entrySet()) {
				rentalService.rentHistoryRepo.put(rentEvent.getKey(), rentEvent.getValue()); 
			}
			break;
		}
	}

}
