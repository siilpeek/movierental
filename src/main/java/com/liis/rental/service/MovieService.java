package com.liis.rental.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.liis.rental.entity.Movie;

@Service
public class MovieService {

	Map<Integer, Movie> movieRepo = new HashMap<>();
	static Logger logger = Logger.getLogger(MovieService.class.getName());

	public Map<Integer, Movie> getMovieRepo() {
		return movieRepo;
	}

	public Movie createMovie(Movie movie) throws StreamWriteException, IOException {
		movie.setId(1 + movieRepo.size());
		movieRepo.put(1 + movieRepo.size(), movie);
		logger.log(Level.INFO, "Added one new movie: " + movie.toString());
		updateMovieValuesForFile();
		return movie;
	}

	public Movie updateMovie(Integer id, Movie movie) {
		movieRepo.remove(id);
		movie.setId(id);
		movie.setTitle(movie.getTitle());
		movie.setDescription(movie.getDescription());
		movie.setActors(movie.getActors());
		movie.setReleaseDate(movie.getReleaseDate());
		movieRepo.put(id, movie);
		logger.log(Level.INFO, "Updated movie with values: " + movie.toString());
		updateMovieValuesForFile();
		return movie;
	}

	public ResponseEntity<?> deleteMovie(Integer id) {
		movieRepo.remove(id);
		logger.log(Level.INFO, "Removed movie with id: " + id);
		updateMovieValuesForFile();
		return ResponseEntity.ok().build();
	}

	public Collection<Movie> getAllMovies() {
		return movieRepo.values();
	}

	public Movie findMovieById(Integer id) {
		return movieRepo.get(id);
	}
	
	private void updateMovieValuesForFile() {
		String dataResource = System.getProperty("datasource");
		if (dataResource == null) {
			return;
		}
		ObjectMapper mapper;
		File file;
		if (dataResource.equals("json")) {
			file = new File("src/main/resources/movie.json");
			mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			try {
				JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
				mapper.writeValue(g, movieRepo);
				g.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (dataResource.equals("yaml")) {
			file = new File("src/main/resources/movie.yaml");
			mapper = new ObjectMapper(new YAMLFactory());
			mapper.findAndRegisterModules();
			try {
				JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
				mapper.writeValue(g, movieRepo);
				g.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
