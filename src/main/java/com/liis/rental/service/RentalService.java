package com.liis.rental.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.liis.rental.entity.Movie;
import com.liis.rental.entity.RentEvent;
import com.liis.rental.entity.User;

@Service
public class RentalService {

	static Logger logger = Logger.getLogger(RentalService.class.getName());
	Map<String, RentEvent> rentHistoryRepo = new HashMap<>();

	@Autowired
	private MovieService movieService;

	@Autowired
	private UserService userService;

	public RentEvent rentMovie(Integer userId, Integer movieId, Integer rentedWeeks) throws Exception{
		if(!movieService.getMovieRepo().containsKey(movieId)) {
			throw new Exception("Movie with Id " + movieId + " not found");
		}
		Movie movie = movieService.getMovieRepo().get(movieId);
		User user = userService.searchUserById(userId);
		LocalDate returnDate = getCurrentDate().plusWeeks(rentedWeeks);
		RentEvent newRentEvent = new RentEvent(rentHistoryRepo.size() + 1, user, getCurrentDate(), returnDate, true, movie, getMoviePrice(returnDate, movie.getReleaseDate(), rentedWeeks));
		rentHistoryRepo.put(userId + "-" + movieId, newRentEvent);
		logger.log(Level.INFO, "User (ID:" + userId + ") lent a movie (ID:" + movieId + "). Created a new rent event: " + newRentEvent.toString());
		updateRentalValuesForFile();
		return newRentEvent;
	}

	private double getMoviePrice(LocalDate returnDate, LocalDate releaseDate, int rentedWeeks) {
		int weeks = getNumberOfWeeksBetweenDates(releaseDate, returnDate);
		double price;

		if (weeks <= 52) {
			price = 5;
		} else if (weeks > 52 && weeks < 156) {
			price = 3.49;
		} else {
			price = 1.99;
		}
		logger.log(Level.INFO, "Price for movie was " + price);
		return price * rentedWeeks;
	}

	private LocalDate getCurrentDate() {
		return LocalDate.now();
	}

	public Integer getInvoice(Integer userId) throws Exception {
		Map<String, RentEvent> filteredActiveRents = rentHistoryRepo.entrySet().stream()
				.filter(user -> user.getValue().getUser().getId() == userId).filter(event -> event.getValue().isActive())
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		int invoiceAmount = calculateUserInvoice(filteredActiveRents.values());
		logger.log(Level.INFO, "User (ID:" + userId + ") got a new invoice for amount:" + invoiceAmount);
		return invoiceAmount;
	}

	public List<Movie> getStatisticsForPopularAndNotPopularMovies(String searchCriteria){
		List<Movie> moviesRentedAtleastOnce = rentHistoryRepo.entrySet().stream().map(e -> e.getValue().getMovie()).collect(Collectors.toList());
		List<Movie> allMovies =  movieService.movieRepo.entrySet().stream().map(e -> e.getValue()).collect(Collectors.toList());

		switch(searchCriteria) {
		case "popular":
			List<Movie> popularMovies = moviesRentedAtleastOnce.stream()
			.filter(two -> allMovies.stream()			    
					.anyMatch(one -> one.getTitle().equals(two.getTitle()) 
							))
			.collect(Collectors.toList());
			logger.info("Popular movies are: " + popularMovies.toString());
			return popularMovies;
		case "notpopular":
			List<Movie> neverRentedMovies = allMovies.stream()
			.filter(two -> !moviesRentedAtleastOnce.stream()			    
					.anyMatch(one -> one.getTitle().equals(two.getTitle()) 
							))
			.collect(Collectors.toList());
			logger.info("Never rented movies are: " + neverRentedMovies.toString());
			return neverRentedMovies;
		default:
			logger.info("Search criteria not supported");
			return allMovies;
		}				
	}

	public double getAveragesForRentalPriceAndRentalTimes(String searchCriteria) {
		switch(searchCriteria) {
		case "rentaltime":
			List<Integer> rentalTimesInWeeks = new ArrayList<>();

			rentHistoryRepo.entrySet().forEach(rentHistory -> {
				rentalTimesInWeeks.add(getNumberOfWeeksBetweenDates(rentHistory.getValue().getBorrowedDate(), rentHistory.getValue().getReturnDate()));
			});

			double averageRentalTime = rentalTimesInWeeks.stream()
					.mapToDouble(rentalTime -> rentalTime)
					.average()
					.orElse(0.0);
			logger.info("Rental times in weeks were: " + rentalTimesInWeeks.toString() + " and average is " + averageRentalTime);
			return averageRentalTime;
		case "rentalprice":
			double averageRentalPrice = rentHistoryRepo.entrySet().stream().map(e -> e.getValue()).mapToDouble(RentEvent::getPrice).average().orElse(0.0);
			logger.info("Average rental price is " + averageRentalPrice);
			return averageRentalPrice;
		default: 
			logger.info("Search criteria not supported");
			return 0.0;
		} 
	}

	public RentEvent returnMovie(Integer userId, Integer movieId) throws Exception {
		if(!movieService.getMovieRepo().containsKey(movieId)) {
			throw new Exception("Movie with Id " + movieId + " not found");
		}
		RentEvent rentEvent = rentHistoryRepo.get(userId + "-" + movieId);
		rentEvent.setActive(false);
		logger.log(Level.INFO, "User (ID" + userId + ") returned movie with ID: " + movieId);
		updateRentalValuesForFile();
		return rentEvent;
	}

	private Integer calculateUserInvoice(Collection<RentEvent> rentals) {
		int rentalSum = 0;
		for(RentEvent event : rentals) {
			rentalSum += event.getPrice();
		}
		return rentalSum;
	}

	public Collection<RentEvent> getAllRentEvents() {
		return rentHistoryRepo.values();
	}


	private void updateRentalValuesForFile() throws FileNotFoundException, IOException {
		String dataResource = System.getProperty("datasource");
		if (dataResource == null) {
			return;
		}
		ObjectMapper mapper;
		File file;
		if (dataResource.equals("json")) {
			file = new File("src/main/resources/rent.json");
			mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
			mapper.writeValue(g, rentHistoryRepo);
			g.close();
		} else if (dataResource.equals("yaml")) {
			file = new File("src/main/resources/rent.yaml");
			mapper = new ObjectMapper(new YAMLFactory());
			mapper.findAndRegisterModules();
			JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
			mapper.writeValue(g, rentHistoryRepo);
			g.close();
		}
		
	}

	private int getNumberOfWeeksBetweenDates(LocalDate releaseDate, LocalDate returnDate) {
		return Math.toIntExact(ChronoUnit.WEEKS.between(releaseDate, returnDate));
	}

}
