# Backend Application for Movie Rental

Here you can find a small backend application for renting movies

## Technical background

Application is written in Java and is using Spring Boot.

## Starting the application, logs and using of yaml and json files

Application can be started by running class "RentalApplication.java". Application is running on default port: http://localhost:8080/ what I am using as well for examples.
Application logs information to console for important operations and services. 

Data can be fed for application with either json or yaml files. Please make sure that all the files are present and correctly constructed before starting application.
Any changes that you wish to do directly in the files have to be done before starting application.
You will need to have one file representing one database table. There are in total four files necessary in either json or yaml format: movie.json/movie.yaml, user.json/user.yaml, category.json/category.yaml and rent.json/rent.yaml
Here you can find example files with example data:
[Link to yaml and json resources](../src/main/resources)

To use data files you need to use program argument to specify wheter you wish to use yaml or json. This can be done for an example by specifying it uder Run Configurations >> Arguments >> VM Arguments like this: -Ddatasource=yaml

## Application overview

Application contains of four model classes: Movie, User, RentEvent and Category. Business logic is divided into four service classes MovieService, UserService, CategoryService and RentalService. Service classes have on helper class StartupApplicationListener. API endpoints are created in classes MovieController, RentalController, UserController and CategoryController. 

The main classes of application are Movie and User. Movie holds information about movies and User holds that of users. Category is a class to categorise movies and RentEvent is created whenever user rents a movie.

## API calls

Here are descriptions for each of the API calls including examples

USER CONTROLLER

### Creating new user

POST http://localhost:8080/user/add This creates one new user
```
{
        "id": 4,
        "firstName": "Mari",
        "lastName": "Maasikas",
        "email": "test@test.ee",
        "address":"Metsa 1",
        "password": "password" ,
        "username": "mari"
}
```
### Get user by id 

GET http://localhost:8080/user/1

### Retrieve all currently existing users:
GET http://localhost:8080/users

MOVIE CONTROLLER

### Add a new movie
POST http://localhost:8080/movie/add This adds one new movie
```
{
        "title": "Forrest Gump",
        "description": "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.",
        "actors":["Tom Hanks", "Robin Wright"],
        "releaseDate": "1994-04-06T19:29:13.683" ,
        "price": 1
 }
```

### Retrieve all currently existing movies
GET http://localhost:8080/movies

### Update movie:
PUT http://localhost:8080//movie/update/1 Updates movie with ID 1 with values from request body

```
{
    "title": "The Shining",
    "description": "A family heads to an isolated hotel for the winter where a sinister presence influences the father into violence, while his psychic son sees horrific forebodings from both past and future",
    "actors": [
        "Jack Nicholson",
        "Shelley Duvall"
    ],
    "releaseDate": "1980-04-06T19:29:13.683",
    "price": 1
}
```

### Raamatute lisamine osakondadesse koos eelnevate näidetega:
DELETE http://localhost:8080/movie/delete/1 Deletes movie with ID 1

RENTAL CONTROLLER

### Renting a movie for user
PUT http://localhost:8080/rent/movie/1/user=1&weeks=2 This call rents movie with ID 1 for user with ID 1 for 2 weeks
PUT http://localhost:8080/rent/movie/2/user=1&weeks=2 This call rents movie with ID 2 for user with ID 1 for 2 weeks

### Returning a movie for user
PUT http://localhost:8080/rent/movie/return/user=1&movie=2 This call returns movie with ID 2 to user with ID 1 (if using previous example, user should have one more movie)

### Creating an inovice for user
GET http://localhost:8080/invoice/user=1 This call returns total amount of for all the movies user has rented (and has not returned yet)

### Retrieve all rent events (active and inactive):
GET http://localhost:8080//renthistory

### Get statistics for popular and not popular movies:
GET http://localhost:8080/rent/statistics/popular This call returns list of all movies that have been rented at least once
GET http://localhost:8080/rent/statistics/notpopular This call returns list of all movies that have never been rented

### Get statistics for average rental prices and times
GET http://localhost:8080/rent/statistics/averages/rentalprice This call returns average price for all movies
GET http://localhost:8080/rent/statistics/averages/rentaltime This call returns average rental time for all movies (in weeks)

CATEGORY CONTROLLER

### Add one new category
POST http://localhost:8080/category/create 
```
{
        "categoryId": 2,
        "name": "scary movies"
    }
```

### Retrieve all currently existing categories and movies if they have been added to categories
GET http://localhost:8080/categories

### Add movie to gategory
PUT http://localhost:8080/category/2/addmovie/4 Adds movie with ID 4 to category 2

# Tests

This application has unit tests for all four controllers. Tests can be found under: /src/test/java/com.liis.rental.

